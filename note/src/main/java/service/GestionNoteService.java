package service;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import persistance.ClassRoom;
import persistance.Module;
import persistance.Note;
import persistance.Student;

/**
 * Session Bean implementation class GestionNoteService
 */
@Stateless
public class GestionNoteService implements GestionNoteServiceRemote {

	@PersistenceContext(unitName = "note")
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public GestionNoteService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean addModule(Module module) {
		try {
			em.persist(module);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean addClassRoom(ClassRoom classRoom) {
		try {
			em.persist(classRoom);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public ClassRoom findClassRoomById(int id) {
		return em.find(ClassRoom.class, id);
	}

	@Override
	public boolean affectStudentsToClassRoom(ClassRoom classRoom,
			List<Student> students) {
		try {
			for (Student student : students) {
				student.setClassRoom(classRoom);
				em.persist(student);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean addnote(int note, Date date, String nom_module, int idStudent) {
		try {
			Student student = em.find(Student.class, idStudent);
			Query query = em
					.createQuery("select m from Module m where m.name=:n");
			query.setParameter("n", nom_module);
			Module mo = (Module) query.getSingleResult();
			Note note2 = new Note(note, student, mo, date);
			em.persist(note2);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Module> findAllModuleByClass(String room) {
		Query query = em
				.createQuery("select c from ClassRoom c where c.name=:n");
		query.setParameter("n", room);
		ClassRoom classRoom = (ClassRoom) query.getSingleResult();
		System.out.println("la classroom est" + classRoom.getName());
		query = em
				.createQuery("select distinct m from Module m join m.notes n join n.student s where s.classRoom=:classR");
		query.setParameter("classR", classRoom);
		return query.getResultList();
	}

}

package service;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import persistance.ClassRoom;
import persistance.Module;
import persistance.Student;

@Remote
public interface GestionNoteServiceRemote {
	public boolean addModule(Module module);

	public boolean addClassRoom(ClassRoom classRoom);

	public ClassRoom findClassRoomById(int id);

	public boolean affectStudentsToClassRoom(ClassRoom classRoom,
			List<Student> students);

	public boolean addnote(int note, Date date, String nom_module, int idStudent);

	List<Module> findAllModuleByClass(String room);
}

package persistance;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ClassRoom
 *
 */
@Entity
@Table(name="esp_class_room")
public class ClassRoom implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idClass;
	private String name;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="classRoom",cascade=CascadeType.ALL)
	private List<Student> students;
	

	public ClassRoom() {
		super();
	}   
	public int getIdClass() {
		return this.idClass;
	}

	public void setIdClass(int idClass) {
		this.idClass = idClass;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
   
}

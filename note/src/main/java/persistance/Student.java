package persistance;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Student
 *
 */
@Entity
@Table(name="esp_student")
public class Student implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idStudent;
	private String name;
	private String lastName;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name="id_class")
	private ClassRoom classRoom;
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
	private List<Note> notes = new ArrayList<Note>();;

	public Student() {
		super();
	}
 
	public Student(String name, String lastName) {
		super();
		this.name = name;
		this.lastName = lastName;
	}

	public int getIdStudent() {
		return this.idStudent;
	}

	public void setIdStudent(int idStudent) {
		this.idStudent = idStudent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ClassRoom getClassRoom() {
		return classRoom;
	}

	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

}

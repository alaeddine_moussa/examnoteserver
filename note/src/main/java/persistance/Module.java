package persistance;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Module
 *
 */
@Entity
@Table(name="esp_module")
public class Module implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idModule;
	@Column(name="name_module")
	private String name;
	private int duration;
	private String type;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="module",cascade=CascadeType.ALL)
	private List<Note> notes = new ArrayList<Note>();
	

	public Module() {
		super();
	}   	
	
	public Module(String name, int duration, String type) {
		super();
		this.name = name;
		this.duration = duration;
		this.type = type;
	}



	public int getIdModule() {
		return this.idModule;
	}

	public void setIdModule(int idModule) {
		this.idModule = idModule;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}   
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public List<Note> getNotes() {
		return notes;
	}
	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
   
}

package persistance;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Note
 *
 */
@Entity
@Table(name = "esp_note")
public class Note implements Serializable {

	@EmbeddedId
	private NoteId noteId;
	private int note;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumn(name = "idStudentPK", insertable = false, updatable = false)
	private Student student;
	@ManyToOne
	@JoinColumn(name = "idModulePK", insertable = false, updatable = false)
	private Module module;

	public Note() {
		super();
	}

	public Note(int note, Student student, Module module, Date date) {
		super();
		this.note = note;
		this.student = student;
		this.module = module;
		this.noteId = new NoteId(student.getIdStudent(), module.getIdModule(),
				date);
	}

	public int getNote() {
		return this.note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public NoteId getNoteId() {
		return noteId;
	}

	public void setNoteId(NoteId noteId) {
		this.noteId = noteId;
	}

}

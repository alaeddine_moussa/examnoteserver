package persistance;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class NoteId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idStudentPK;
	private int idModulePk;
	private Date date;

	public NoteId() {
	}

	public NoteId(int idStudentPK, int idModulePk, Date date) {
		super();
		this.idStudentPK = idStudentPK;
		this.idModulePk = idModulePk;
		this.date = date;
	}

	public int getIdStudentPK() {
		return idStudentPK;
	}

	public void setIdStudentPK(int idStudentPK) {
		this.idStudentPK = idStudentPK;
	}

	public int getIdModulePk() {
		return idModulePk;
	}

	public void setIdModulePk(int idModulePk) {
		this.idModulePk = idModulePk;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idModulePk;
		result = prime * result + idStudentPK;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoteId other = (NoteId) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idModulePk != other.idModulePk)
			return false;
		if (idStudentPK != other.idStudentPK)
			return false;
		return true;
	}

}
